var tokenizer = require("sbd");
import toastr from "toastr";

for (var i = 0; i < 10; i++) {
  toastr.success("I am loaded by ES6 Right");
}

const need = "World Peace";
console.log(`I want to see ${need}`);
const sum = (arr) => {
  let sum = 0;
  for (const ele of arr) {
    sum += ele;
  }
  console.log(`The numbers to add are: ${arr}`);
  console.log(`And their sum is ${sum}`);
};
sum([10, 20, 30, 40, 50, 23]);

var optional_options = {};
var text =
  "On Jan. 20, former Sen. Barack Obama became the 44th President of the U.S. Millions attended the Inauguration.";
var sentences = tokenizer.sentences(text, optional_options);

console.log(sentences);
// [
//  'On Jan. 20, former Sen. Barack Obama became the 44th President of the U.S.',
//  'Millions attended the Inauguration.',
// ]
